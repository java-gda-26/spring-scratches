package pl.sdacademy.springscratches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class SpringScratchesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringScratchesApplication.class, args);
    }
}
