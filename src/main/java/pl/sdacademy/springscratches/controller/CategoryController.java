package pl.sdacademy.springscratches.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sdacademy.springscratches.dto.CategoryDto;
import pl.sdacademy.springscratches.service.CategoryService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    public String list(Model model) {
        model.addAttribute("newCategory", new CategoryDto());
        model.addAttribute("categories", categoryService.findAll());
        return "categories";
    }

    @PostMapping
    public String create(CategoryDto category) {
        categoryService.create(category);
        return "redirect:/categories";
    }
}
