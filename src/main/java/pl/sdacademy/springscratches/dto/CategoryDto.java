package pl.sdacademy.springscratches.dto;

import lombok.Data;

@Data
public class CategoryDto {

    private Long id;

    private Long parentId;

    private String name;
}
