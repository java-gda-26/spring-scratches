package pl.sdacademy.springscratches.dto;

public enum MessageType {
    SMS, EMAIL
}
