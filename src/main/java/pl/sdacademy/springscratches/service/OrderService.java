package pl.sdacademy.springscratches.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sdacademy.springscratches.dto.MessageDto;
import pl.sdacademy.springscratches.dto.MessageType;
import pl.sdacademy.springscratches.dto.NotificationType;
import pl.sdacademy.springscratches.dto.UserDto;

@RequiredArgsConstructor
@Service
public class OrderService {

    private final NotificationService notificationService;

    public void createOrder(UserDto user) {
        MessageDto message = new MessageDto();
        message.setMessageType(MessageType.SMS);
        message.setNotificationType(NotificationType.ORDER_CREATED_CONFIRMATION);
        message.setRecipient(user);
        notificationService.notify();
    }

    public void completeOrder(UserDto user) {
        MessageDto message = new MessageDto();
        message.setMessageType(MessageType.SMS);
        message.setNotificationType(NotificationType.ORDER_COMPLETED_CONFIRMATION);
        message.setRecipient(user);
    }
}
