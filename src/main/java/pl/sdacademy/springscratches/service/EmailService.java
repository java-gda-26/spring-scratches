package pl.sdacademy.springscratches.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;

@RequiredArgsConstructor
@Slf4j
@Service
public class EmailService implements MessageService {

    private final JavaMailSender mailSender;
    private final TemplateEngine templateEngine;

    @Override
    public void sendMessage(String recipient, String message) {
        log.info("Sending message \"{}\" to email {}", message, recipient);

        Context context = new Context();
        context.setVariable("message", message);
        String emailBody = templateEngine.process("emailMessage", context);

        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
                helper.setFrom("test@test.com");
                helper.setSubject("Message from Spring Application");
                helper.setTo(recipient);
                helper.setText(emailBody, true);
            }
        };

        mailSender.send(preparator);
    }
}
