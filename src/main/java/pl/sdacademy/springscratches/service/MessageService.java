package pl.sdacademy.springscratches.service;

public interface MessageService {

    void sendMessage(String receipient, String message);
}
