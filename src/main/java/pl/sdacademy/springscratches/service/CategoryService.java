package pl.sdacademy.springscratches.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sdacademy.springscratches.CategoryRepository;
import pl.sdacademy.springscratches.dto.CategoryDto;
import pl.sdacademy.springscratches.entity.Category;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public List<Category> findAll() {
        return categoryRepository.findAllByParentIsNull();
    }

    public void create(CategoryDto newCategory) {
        Category entity = map(newCategory);
        categoryRepository.save(entity);
    }

    private Category map(CategoryDto dto) {
        Category category = new Category();
        category.setName(dto.getName());

        if (dto.getParentId() != null) {
            Category parent = categoryRepository.findById(dto.getParentId())
                    .orElseThrow((() -> new IllegalArgumentException("Parent category not found")));
            category.setParent(parent);
        }

        return category;
    }
}
