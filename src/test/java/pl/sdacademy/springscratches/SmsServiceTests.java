package pl.sdacademy.springscratches;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.sdacademy.springscratches.service.SmsService;

@SpringBootTest
class SmsServiceTests {

    @Autowired
    private SmsService smsService;

    @Test
    void testSendSms() throws Exception {
        // given
        String recipient = "501123456";
        String messageBody = "Test message";

        // when
        smsService.sendMessage(recipient, messageBody);

        // then
    }

}
