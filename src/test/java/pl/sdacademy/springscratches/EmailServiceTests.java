package pl.sdacademy.springscratches;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.sdacademy.springscratches.service.EmailService;

@SpringBootTest
public class EmailServiceTests {

    @Autowired
    private EmailService emailService;

    @Test
    public void testSendEmail() throws Exception {
        // given
        String recipient = "email@test.com";
        String message = "Test email message";

        // when
        emailService.sendMessage(recipient, message);

        // then
    }
}
